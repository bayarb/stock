package com.bbayar.stock.base;

import android.app.Activity;
import android.content.Context;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.bbayar.stock.ui.dialog.LoadDialog;

import butterknife.ButterKnife;

/**
 * Created by qqq on 02.02.2017.
 */

public abstract class BaseActivity extends AppCompatActivity {

    private LoadDialog dialog;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(layoutResID);
        ButterKnife.bind(this);
    }

    public void setUpToolbar(Toolbar toolbar, boolean isButtonVisible) {
        setSupportActionBar(toolbar);
        if (isButtonVisible) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        } else {
            getSupportActionBar().setHomeButtonEnabled(false);
        }
    }

    public void setTitle(String title) {
        if (title != null) {
            getSupportActionBar().setTitle(title);
        } else {
            getSupportActionBar().setTitle("");
        }
    }

    public void hideKeyboard() {
        InputMethodManager inputMethodManager = ((InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE));
        inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus().getWindowToken(), 0);
    }

    public void replaceFragment(int idContainer, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(idContainer, fragment, fragment.getClass().getName())
                .commit();
    }

    public void addFragment(int idContainer, Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(idContainer, fragment, fragment.getClass().getName())
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }

    public void clearBackStack() {
        int stackSize = getSupportFragmentManager().getBackStackEntryCount();
        for (int i = 0; i < stackSize ; i++) {
            getSupportFragmentManager().popBackStack();
        }
    }

    public boolean isOnline() {
        ConnectivityManager cm = ((ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE));
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

    public boolean isGpsActive() {
        LocationManager lm = ((LocationManager) this.getSystemService(Context.LOCATION_SERVICE));
        if (!lm.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            return false;
        } else {
            return true;
        }
    }

    public void showDialogProgress() {
        dialog = new LoadDialog();
        dialog.show(getSupportFragmentManager(), LoadDialog.class.getName());
    }

    public void dismissDialogProgress() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
