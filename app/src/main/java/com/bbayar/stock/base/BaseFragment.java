package com.bbayar.stock.base;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Created by qqq on 02.02.2017.
 */

public abstract class BaseFragment extends Fragment {

    private Unbinder unbinder;

    protected abstract void initViews();

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        unbinder = ButterKnife.bind(this, view);
        initViews();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    public void hideKeyboard() {
        if (getActivity() != null) {
            InputMethodManager inputMethodManager = ((InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE));
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void setTitle(String title) {
        if (getActivity() != null) {
            if (title != null) {
                ((BaseActivity) getActivity()).getSupportActionBar().setTitle(title);
            } else {
                ((BaseActivity) getActivity()).getSupportActionBar().setTitle("");
            }
        }
    }

    public void replaceFragment(int idContainer, Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(idContainer, fragment, fragment.getClass().getName())
                .commit();
    }

    public void replaceFragmentWithBackStack(int idContainer, Fragment fragment) {
        getFragmentManager().beginTransaction()
                .replace(idContainer, fragment, fragment.getClass().getName())
                .addToBackStack(fragment.getClass().getName())
                .commit();
    }

    public void showDialogProgress() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).showDialogProgress();
        }
    }

    public void dismissDialogProgress() {
        if (getActivity() != null) {
            ((BaseActivity) getActivity()).dismissDialogProgress();
        }
    }
}
