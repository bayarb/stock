package com.bbayar.stock.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by qqq on 08.02.2017.
 */

public class ModelFiller {

    private List<Movie> movies = new ArrayList<>();

    public List<Movie> getModel() {
        fillModel();
        return movies;
    }

    private void fillModel() {
        for (int i = 1; i < 5; i++) {
            Movie avenger = new Movie("Avengers " + i, "asd", "action", (150 + i * 10), true, "USA");
            movies.add(avenger);
            Movie fastAndFurious = new Movie("The Fast and the Furious " + i, "zxc", "criminal", (100 + i * 15), true, "Canada");
            movies.add(fastAndFurious);
            Movie finalDestination = new Movie("Final Destination " + i, "qwe", "comedy", (150 + i * 5), false, "Australia");
            movies.add(finalDestination);
        }
    }

}
