package com.bbayar.stock.model;

/**
 * Created by qqq on 08.02.2017.
 */
public class Movie {

    private String name;
    private String description;
    private String genre;
    private int price;
    private boolean status;
    private String country;

    public Movie(String name, String description, String genre, int price, boolean status, String country) {
        this.name = name;
        this.description = description;
        this.genre = genre;
        this.price = price;
        this.status = status;
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
