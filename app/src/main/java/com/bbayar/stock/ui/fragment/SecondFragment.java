package com.bbayar.stock.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseFragment;
import com.bbayar.stock.model.ModelFiller;
import com.bbayar.stock.model.Movie;
import com.bbayar.stock.ui.adapter.MoviesAdapter;

import java.util.List;

import butterknife.BindView;

/**
 * Created by qqq on 07.02.2017.
 */
public class SecondFragment extends BaseFragment {

    @BindView(R.id.fs_recycler_view)
    RecyclerView recyclerView;

    private List<Movie> movies;

    public static SecondFragment newInstance() {
        return new SecondFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    @Override
    protected void initViews() {
        ModelFiller modelFiller = new ModelFiller();
        movies = modelFiller.getModel();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(new MoviesAdapter(movies));
    }

}
