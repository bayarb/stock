package com.bbayar.stock.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.TextView;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseActivity;
import com.bbayar.stock.ui.adapter.ExamplePagerAdapter;
import com.bbayar.stock.ui.widget.CustomViewPager;

import butterknife.BindView;
import butterknife.OnClick;

public class TabExampleActivity extends BaseActivity {

    @BindView(R.id.ate_custom_view_pager)
    CustomViewPager viewPager;

    private ExamplePagerAdapter adapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab_example);

        adapter = new ExamplePagerAdapter(getSupportFragmentManager());
        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(adapter);
    }

    @OnClick({R.id.ate_tv_first, R.id.ate_tv_second})
    public void onClick(TextView textView) {
        switch (textView.getId()) {
            case R.id.ate_tv_first: viewPager.setCurrentItem(0); break;
            case R.id.ate_tv_second: viewPager.setCurrentItem(1); break;
        }
    }
}
