package com.bbayar.stock.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bbayar.stock.R;
import com.bbayar.stock.model.Movie;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by qqq on 08.02.2017.
 */
public class MoviesAdapter extends RecyclerView.Adapter<MoviesAdapter.MoviesViewHolder> {

    private List<Movie> movies;

    public MoviesAdapter(List<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public MoviesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_movie, parent, false);
        return new MoviesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MoviesViewHolder holder, int position) {
        holder.bindMovie(movies.get(position));
    }

    @Override
    public int getItemCount() {
        return movies.size();
    }

    class MoviesViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.item_name)
        TextView tvName;

        @BindView(R.id.item_genre)
        TextView tvGenre;

        @BindView(R.id.item_price)
        TextView tvPrice;

        @BindView(R.id.item_status)
        TextView tvStatus;

        public MoviesViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindMovie(Movie movie) {
            tvName.setText(movie.getName());
            tvGenre.setText(movie.getGenre());
            tvPrice.setText(String.valueOf(movie.getPrice()));
            if (movie.getStatus()) {
                tvStatus.setText("Active");
            } else {
                tvStatus.setText("Non-active");
            }
        }

    }
}
