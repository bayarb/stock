package com.bbayar.stock.ui.activity;

import android.os.Bundle;
import android.widget.EditText;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class ValidationActivity extends BaseActivity {

    @BindView(R.id.av_et_email)
    EditText etEmail;

    @BindView(R.id.av_et_password)
    EditText etPassword;

    @BindView(R.id.av_et_username)
    EditText etUsername;

    @BindView(R.id.av_et_phone)
    EditText etPhone;

    String email, password, username, phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_validation);
    }

    @OnClick(R.id.av_btn_validate)
    public void onValidateClicked() {
        email = etEmail.getText().toString();
        password = etPassword.getText().toString();
        username = etUsername.getText().toString();
        phone = etPhone.getText().toString();
    }
}
