package com.bbayar.stock.ui.activity;

import android.os.Bundle;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseActivity;

public class StyledEditTextActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_styled_edit_text);
    }
}
