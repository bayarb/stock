package com.bbayar.stock.ui.dialog;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bbayar.stock.R;

/**
 * Created by qqq on 06.02.2017.
 */
public class LoadDialog extends DialogFragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_loading, container, true);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        setCancelable(false);
        return view;
    }
}
