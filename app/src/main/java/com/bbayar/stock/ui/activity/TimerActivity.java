package com.bbayar.stock.ui.activity;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.widget.TextView;
import android.widget.Toast;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class TimerActivity extends BaseActivity {

    @BindView(R.id.at_tv_ready_indicator)
    TextView tvReadyIndicator;

    private CountDownTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_timer);
        tvReadyIndicator.setText("NOPE");
    }


    @OnClick(R.id.at_btn_start_service)
    public void onStartServiceClicked() {
        tvReadyIndicator.setText("NOPE");
        if (timer != null) {
            timer.cancel();
        }
        // re-schedule timer here
        // otherwise, IllegalStateException of
        // "TimerTask is scheduled already"
        // will be thrown
        timer = new CountDownTimer(30000, 5000) {
            @Override
            public void onTick(long l) {
                Toast.makeText(TimerActivity.this, String.valueOf(l), Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinish() {
                tvReadyIndicator.setText("TRUE");
            }
        }.start();
    }
}
