package com.bbayar.stock.ui.widget;


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class CustomViewPager extends ViewPager {

    private boolean flag;

    public CustomViewPager(Context context) {
        super(context);
    }

    public CustomViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return flag ? super.onTouchEvent(ev) : false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return flag ? super.onInterceptTouchEvent(ev) : false;
    }

    public void setPagingEnabled(boolean flag) {
        this.flag = flag;
    }

    public boolean isPagingEnabled() {
        return flag;
    }
}
