package com.bbayar.stock.ui.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseFragment;
import com.bbayar.stock.ui.activity.MaskedActivity;
import com.bbayar.stock.ui.activity.StyledEditTextActivity;
import com.bbayar.stock.ui.activity.TabExampleActivity;
import com.bbayar.stock.ui.activity.TimerActivity;
import com.bbayar.stock.ui.activity.ValidationActivity;
import com.bbayar.stock.ui.activity.ViewPagerActivity;

import butterknife.OnClick;

/**
 * Created by qqq on 07.02.2017.
 */
public class FirstFragment extends BaseFragment {

    public static Fragment newInstance() {
        return new FirstFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_first, container, false);
    }

    @Override
    protected void initViews() {

    }

    @OnClick(R.id.ff_btn_show_second_fragment)
    public void onShowSecondFragmentClicked() {
        replaceFragmentWithBackStack(R.id.am_fragment_container, SecondFragment.newInstance());
    }

    @OnClick(R.id.ff_btn_show_third_fragment)
    public void onShowThirdFragmentClicked() {
        replaceFragmentWithBackStack(R.id.am_fragment_container, ThirdFragment.newInstance());
    }

    @OnClick(R.id.ff_btn_show_validation_example)
    public void onShowValidationExampleClicked() {
        startActivity(new Intent(getContext(), ValidationActivity.class));
    }

    @OnClick(R.id.ff_btn_show_styled_et)
    public void onShowStyledEditTextClicked() {
        startActivity(new Intent(getContext(), StyledEditTextActivity.class));
    }

    @OnClick(R.id.ff_btn_show_tab_example)
    public void onShowTabExampleClicked() {
        startActivity(new Intent(getContext(), TabExampleActivity.class));
    }

    @OnClick(R.id.ff_btn_show_avp)
    public void onAvpClicked() {
        startActivity(new Intent(getContext(), ViewPagerActivity.class));
    }

    @OnClick(R.id.ff_btn_show_mask)
    public void onMaskClicked() {
        startActivity(new Intent(getContext(), MaskedActivity.class));
    }

    @OnClick(R.id.ff_btn_show_timer_example)
    public void onTimerClicked() {
        startActivity(new Intent(getContext(), TimerActivity.class));
    }
}
