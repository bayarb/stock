package com.bbayar.stock.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseFragment;

import butterknife.BindView;
import butterknife.OnClick;

/**
 * Created by qqq on 07.02.2017.
 */
public class ThirdFragment extends BaseFragment {

    @BindView(R.id.ft_tv_counter)
    TextView tvCounter;

    private int counter = 0;

    public static Fragment newInstance() {
        return new ThirdFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_third, container, false);
    }

    @Override
    protected void initViews() {
        tvCounter.setText(String.valueOf(counter));
    }

    @OnClick(R.id.ft_btn_count)
    public void onCountClicked() {
        tvCounter.setText(String.valueOf(++counter));
    }
}
