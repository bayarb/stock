package com.bbayar.stock.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.bbayar.stock.ui.fragment.SecondFragment;
import com.bbayar.stock.ui.fragment.ThirdFragment;

public class ExamplePagerAdapter extends FragmentPagerAdapter {

    private final int PAGE_COUNT = 2;
    private String tabTitles[] = {"FirstFragment", "SecondFragment"};

    public ExamplePagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0: fragment = SecondFragment.newInstance(); break;
            case 1: fragment = ThirdFragment.newInstance(); break;
            default: fragment = null;
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return PAGE_COUNT;
    }
}
