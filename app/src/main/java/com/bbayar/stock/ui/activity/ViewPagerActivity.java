package com.bbayar.stock.ui.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseActivity;

import butterknife.BindView;

public class ViewPagerActivity extends BaseActivity {

    @BindView(R.id.avp_view_pager)
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_pager);
    }
}
