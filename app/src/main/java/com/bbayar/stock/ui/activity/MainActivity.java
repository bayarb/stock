package com.bbayar.stock.ui.activity;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.bbayar.stock.R;
import com.bbayar.stock.base.BaseActivity;
import com.bbayar.stock.ui.fragment.FirstFragment;

import butterknife.BindView;

public class MainActivity extends BaseActivity {

    @BindView(R.id.am_toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setUpToolbar(toolbar, false);
        replaceFragment(R.id.am_fragment_container, FirstFragment.newInstance());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
}
